# Diatrofi

## Status
[![pipeline status](https://gitlab.com/diatrofi/diatrofi/badges/master/pipeline.svg)](https://gitlab.com/diatrofi/diatrofi/commits/master)
[![coverage report](https://gitlab.com/diatrofi/diatrofi/badges/master/coverage.svg)](https://gitlab.com/diatrofi/diatrofi/commits/master)


## Deskripsi
Diatrofi merupakan chatbot berbasis applikasi line.  
Tujuan utama dari Diatrofi adalah menata pola hidup seperti olahraga, makan, dan jam tidur.

## Developer

- Igor Lestin
- Ervan Haryadi
- Azhar Difa Arnanda
- Bramanta Nararya
- Atallah Annafis

**Cara menambahkan Diatrofi sebagai teman!**

![Barcode Diatrofi](https://qr-official.line.me/M/mOHnviY3p0.png)

1. Scan barcode berikut dengan menggunakan LINE
2. Tekan tombol "Tambahkan Sebagai Teman" pada ponsel Anda
3. Selamat, Anda berhasil menambahkan Diatrofi sebagai teman 

## Miscellaneous

### Saat port lokal terpakai padahal seharusnya tidak

1. Buka "Command Prompt" dengan Administrator
2. Ketik "netstat -ano|findstr:"
3. Cari PID dari port (atau task) yang hendak dimatikan
4. Ketik "taskkill /PID [Nomor PID task] /F"
5. Jalankan Spring Boot App kembali

### Cek kebenaran gitlab-ci.yml ###

Gunakan link "https://gitlab.com/{Username\_or\_GroupName}/{Nama_Project}/-/ci/lint"