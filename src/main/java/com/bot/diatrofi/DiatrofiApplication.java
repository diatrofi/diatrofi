package com.bot.diatrofi;

import java.util.concurrent.ExecutionException;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

import com.bot.diatrofi.PolaMakanController;

import com.bot.diatrofi.menu.delegation.DelegateBMI;
import com.bot.diatrofi.menu.delegation.BmiData;
import com.bot.diatrofi.menu.delegation.DelegateHealthMeter;
import com.bot.diatrofi.menu.delegation.DelegatePolaHidup;
import com.bot.diatrofi.menu.delegation.DiatrofiDelegate;
import com.bot.diatrofi.menu.delegation.DiatrofiHelpCommand;
import com.bot.diatrofi.menu.delegation.DelegateSetPolaMakan;
import com.bot.diatrofi.menu.delegation.QuickReplySetPolaMakan;
import com.linecorp.bot.client.LineMessagingClient;
import com.linecorp.bot.model.ReplyMessage;
import com.linecorp.bot.model.event.MessageEvent;
import com.linecorp.bot.model.event.message.TextMessageContent;
import com.linecorp.bot.model.message.ImageMessage;
import com.linecorp.bot.model.message.TextMessage;
import com.linecorp.bot.spring.boot.annotation.EventMapping;
import com.linecorp.bot.spring.boot.annotation.LineMessageHandler;

@SpringBootApplication
@LineMessageHandler
public class DiatrofiApplication extends SpringBootServletInitializer {

    DiatrofiDelegate commandReader;
    String userId;
    int varTmpOlahraga;
    int varTmpTidur;
    BmiData bmidata = new BmiData();
    // flag for set pola Hidup, especially set pola makan
    boolean flagPolaHidup = false;
    boolean flagPolaMakan = false;
    // controller untuk menyimpan data pola polamakan
    PolaMakanController polaMakanController = new PolaMakanController();

    boolean flagPolaTidur = false;
    boolean flagPolaOlahraga = false;
    boolean finalAnswerOlahraga = false;

    // Delegate Pola Hidup object
    DelegatePolaHidup delegatePolaHidup = new DelegatePolaHidup();
    DelegateHealthMeter health = new DelegateHealthMeter();

    @Autowired
    private LineMessagingClient lineMessagingClient;

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
        return builder.sources(DiatrofiApplication.class);
    }

    public static void main(String[] args) {
        SpringApplication.run(DiatrofiApplication.class, args);
    }

    @EventMapping
    public void handleTextEvent(MessageEvent<TextMessageContent> messageEvent)
            throws ExecutionException, InterruptedException {
        String message = messageEvent.getMessage().getText().toLowerCase();
        userId = messageEvent.getSource().getUserId();
        String[] splitMessage = message.split(" ");
        String replyToken = messageEvent.getReplyToken();
        String answer = "";
        

        if (flagPolaMakan) {
            if (message.equals("1") || message.equals("2") ||
                message.equals("3") || message.equals("4") || message.equals("5")) {
                polaMakanController.setFrekuensiMakan(Integer.parseInt(message));
                replyQuickReplyDaftarMakanan(replyToken);
            }
            else {
                if (!message.equals("selesai")) {
                    polaMakanController.addMeal(message);
                    replyQuickReplyDaftarMakanan(replyToken);
                }
                else {
                    flagPolaMakan = false;
                }
            }
        } 
        else if (flagPolaOlahraga) {
               /*
            * add value to user input
            */
            if (message.equals("kurang dari 2 jam")) {
                varTmpOlahraga = 1;
                health.setOlahraga(varTmpOlahraga);
                health.setImage(userId, varTmpTidur, varTmpOlahraga);
            } else if (message.equals("2 hingga 4 jam")) {
                varTmpOlahraga = 3;
                health.setOlahraga(varTmpOlahraga);
                health.setImage(userId, varTmpTidur, varTmpOlahraga);
            } else if (message.equals("lebih dari 4 jam")) {
                varTmpOlahraga = 5;
                health.setOlahraga(varTmpOlahraga);
                health.setImage(userId, varTmpTidur, varTmpOlahraga);
            }
            TextMessage jenisOlahraga = delegatePolaHidup.SetJenisOlahraga();
            String tokenWaktuOlahraga = messageEvent.getReplyToken();
            replyChatWithTextMessage(tokenWaktuOlahraga, jenisOlahraga);

            flagPolaOlahraga = false;
            finalAnswerOlahraga = true;
        } else if (finalAnswerOlahraga) {
            String finalAnswer = delegatePolaHidup.FinalAnswerOlahraga();
            replyChat(replyToken, finalAnswer);
            finalAnswerOlahraga = false;
        } else if (flagPolaTidur) {
            if (message.equals("kurang dari 4 jam")) {
                varTmpTidur = 3;
                health.setTidur(varTmpTidur);
                health.setImage(userId, varTmpTidur, varTmpOlahraga);
                String finalAnswer = delegatePolaHidup.PrintPolaTidurKurang4jam();
                replyChat(replyToken, finalAnswer);
            } else if (message.equals("4 hingga 7 jam")) {
                varTmpTidur = 6;
                health.setTidur(varTmpTidur);
                health.setImage(userId, varTmpTidur, varTmpOlahraga);
                String finalAnswer = delegatePolaHidup.PrintPolaTidur4Hingga7jam();
                replyChat(replyToken, finalAnswer);
            } else if (message.equals("lebih dari 7 jam")) {
                varTmpTidur = 8;
                health.setTidur(varTmpTidur);
                health.setImage(userId, varTmpTidur, varTmpOlahraga);
                String finalAnswer = delegatePolaHidup.PrintPolaTidurLebih7jam();
                replyChat(replyToken, finalAnswer);
            }

            flagPolaTidur = false;
            // health.setTidur(varTmpTidur);
        }

        else {
            answer = delegateCommandEvent(splitMessage, userId);

            if (answer.contains("https")) {
                replyImage(replyToken, answer);
            } else if (answer.contains("olahraga")) {
                TextMessage jamOlahraga = delegatePolaHidup.SetPolaOlahraga();
                String tokenJamOlahraga = messageEvent.getReplyToken();
                replyChatWithTextMessage(tokenJamOlahraga, jamOlahraga);
                flagPolaOlahraga = true;
            } else if (answer.contains("tidur")) {
                TextMessage jamTidur = delegatePolaHidup.SetPolaTidur();
                String tokenJamTidur = messageEvent.getReplyToken();
                replyChatWithTextMessage(tokenJamTidur, jamTidur);
                flagPolaTidur = true;
            } else if (answer.contains("makan")) {
                replyQuickReplyFrekuensiMakan(replyToken);
                flagPolaMakan = true;
            } else {
                replyChat(replyToken, answer);
            }
        }
    }

    private String delegateCommandEvent(String[] command, String userId) {
        if (command[0].equals("/help")) {
            commandReader = new DiatrofiHelpCommand(command);
            return commandReader.replyMessage();
        } else if (command[0].equals("/lihat")) {
            if (command[1].equals("bmi")) {
                commandReader = new DelegateBMI(command, userId, bmidata);
            }
            return commandReader.replyMessage();
        } else if (command[0].equals("/health-o-meter")) {
            health.readMessage();
            return health.replyMessage();
        } else if (command[0].equals("/set")) {
            if (command[1].equals("bmi")) {
                commandReader = new DelegateBMI(command, userId, bmidata);
            } else if (command[1].equals("pola")) {
                if (command[2].contains("olahraga")) {
                    return "olahraga";
                } else if (command[2].contains("tidur")) {
                    return "tidur";
                } else if (command[2].equals("makan")) {
                    return "makan";
                }
            }
            return commandReader.replyMessage();
        } else if (command[0].equals("/berat")) {
            commandReader = new DelegateBMI(command, userId, bmidata);
            return commandReader.replyMessage();
        } else if (command[0].equals("/tinggi")) {
            commandReader = new DelegateBMI(command, userId, bmidata);
            return commandReader.replyMessage();
        } else {
            return "Ugh, perintah tidak dimengerti...";
        }
    }

    private void replyChat(String replyToken, String answer) {
        TextMessage answerInMessageForm = new TextMessage(
                answer); /* TextMessage (Ya atau Tidak) -> pesan yang akan dikirim */
        try {
            lineMessagingClient.replyMessage(new ReplyMessage(replyToken, answerInMessageForm)).get();
        } catch (InterruptedException | ExecutionException e) {
            System.out.println("Ada error saat ingin membalas chat");
        }
    }

    private void replyChatWithTextMessage(String replyToken, TextMessage jawaban) {
        try {
            lineMessagingClient.replyMessage(new ReplyMessage(replyToken, jawaban)).get();
        } catch (InterruptedException | ExecutionException e) {
            System.out.println("Ada error saat ingin membalas chat");
        }
    }

    /**
     * Method untuk reply image
     */
    private void replyImage(String replyToken, String url) {
        ImageMessage image = new ImageMessage(url, url);
        try {
            lineMessagingClient.replyMessage(new ReplyMessage(replyToken, image)).get();
        } catch (InterruptedException | ExecutionException e) {
            System.out.println("Cant reply image");
        }
    }

    private void replyQuickReplyDaftarMakanan(String replyToken) {
        QuickReplySetPolaMakan setpolamakanQR = new QuickReplySetPolaMakan();
        TextMessage quickreplyToUser = setpolamakanQR.constructDaftarMakanan();
        try {
            lineMessagingClient.replyMessage(new ReplyMessage(replyToken, quickreplyToUser)).get();
        } catch (InterruptedException | ExecutionException e) {
            System.out.println("Cant send quickreply");
        }
    }

    private void replyQuickReplyFrekuensiMakan(String replyToken) {
        QuickReplySetPolaMakan setpolamakanQR = new QuickReplySetPolaMakan();
        TextMessage quickreplyToUser = setpolamakanQR.constructFrekuensiMakan();
        try {
            lineMessagingClient.replyMessage(new ReplyMessage(replyToken, quickreplyToUser)).get();
        } catch (InterruptedException | ExecutionException e) {
            System.out.println("Cant send quickreply");
        }
    }
}
