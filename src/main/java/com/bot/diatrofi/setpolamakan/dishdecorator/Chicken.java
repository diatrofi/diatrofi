package com.bot.diatrofi.setpolamakan.dishdecorator;
import com.bot.diatrofi.setpolamakan.dishdecorator.DishDecorator;
import com.bot.diatrofi.setpolamakan.Meal;

public class Chicken extends DishDecorator {

    public Chicken(Meal meal) {
        this.meal = meal;
    }

    @Override
    public double calories() {
        return 239.0 + meal.calories();
    }

    public String getDescription() {
		return meal.getDescription() + ", Ayam";
	}
}
