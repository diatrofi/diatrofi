package com.bot.diatrofi.setpolamakan.dishdecorator;
import com.bot.diatrofi.setpolamakan.dishdecorator.DishDecorator;
import com.bot.diatrofi.setpolamakan.Meal;

public class Noodle extends DishDecorator {

    public Noodle(Meal meal) {
        this.meal = meal;
    }

    @Override
    public double calories() {
        return 138.0 + meal.calories();
    }

    public String getDescription() {
		return meal.getDescription() + ", Mie";
	}
}
