package com.bot.diatrofi.setpolamakan.dishdecorator;
import com.bot.diatrofi.setpolamakan.dishdecorator.DishDecorator;
import com.bot.diatrofi.setpolamakan.Meal;

public class Potato extends DishDecorator {

    public Potato(Meal meal) {
        this.meal = meal;
    }

    @Override
    public double calories() {
        return 89.0 + meal.calories();
    }

    public String getDescription() {
		return meal.getDescription() + ", Kentang";
	}
}
