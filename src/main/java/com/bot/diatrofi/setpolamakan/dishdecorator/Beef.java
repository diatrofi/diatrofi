package com.bot.diatrofi.setpolamakan.dishdecorator;
import com.bot.diatrofi.setpolamakan.dishdecorator.DishDecorator;
import com.bot.diatrofi.setpolamakan.Meal;

public class Beef extends DishDecorator {

    public Beef(Meal meal) {
        this.meal = meal;
    }

    @Override
    public double calories() {
        return 250.0 + meal.calories();
    }

    public String getDescription() {
		return meal.getDescription() + ", Daging Sapi";
	}
}
