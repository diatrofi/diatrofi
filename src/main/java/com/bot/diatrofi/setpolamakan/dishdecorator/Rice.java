package com.bot.diatrofi.setpolamakan.dishdecorator;
import com.bot.diatrofi.setpolamakan.dishdecorator.DishDecorator;
import com.bot.diatrofi.setpolamakan.Meal;

public class Rice extends DishDecorator {

    public Rice(Meal meal) {
        this.meal = meal;
    }

    @Override
    public double calories() {
        return 175.0 + meal.calories();
    }

    public String getDescription() {
		return meal.getDescription() + ", Nasi";
	}
}
