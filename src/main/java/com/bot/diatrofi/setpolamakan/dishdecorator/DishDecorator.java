package com.bot.diatrofi.setpolamakan.dishdecorator;
import com.bot.diatrofi.setpolamakan.Meal;
import com.bot.diatrofi.setpolamakan.Dish;

public abstract class DishDecorator extends Dish {

    Meal meal;

    public abstract String getDescription();
}
