package com.bot.diatrofi.setpolamakan.dishdecorator;
import com.bot.diatrofi.setpolamakan.dishdecorator.DishDecorator;
import com.bot.diatrofi.setpolamakan.Meal;

public class Fish extends DishDecorator {

    public Fish(Meal meal) {
        this.meal = meal;
    }

    @Override
    public double calories() {
        return 122.0 + meal.calories();
    }

    public String getDescription() {
		return meal.getDescription() + ", Ikan";
	}
}
