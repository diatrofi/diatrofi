package com.bot.diatrofi.setpolamakan.dishdecorator;
import com.bot.diatrofi.setpolamakan.dishdecorator.DishDecorator;
import com.bot.diatrofi.setpolamakan.Meal;

public class Lettuce extends DishDecorator {

    public Lettuce(Meal meal) {
        this.meal = meal;
    }

    @Override
    public double calories() {
        return 14.0 + meal.calories();
    }

    public String getDescription() {
		return meal.getDescription() + ", Selada";
	}
}
