package com.bot.diatrofi.setpolamakan;

public abstract class Meal {
    protected String description = "Unidentified Food";

    public String getDescription() {
        return description;
    }

    public abstract double calories();
}
