package com.bot.diatrofi.menu.delegation;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.concurrent.ExecutionException;
import java.util.Random;
import java.util.List;
import static java.util.Arrays.asList;
import java.net.URI;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

import com.linecorp.bot.client.LineMessagingClient;
import com.linecorp.bot.model.ReplyMessage;
import com.linecorp.bot.model.event.MessageEvent;
import com.linecorp.bot.model.event.message.TextMessageContent;
import com.linecorp.bot.model.message.TextMessage;
import com.linecorp.bot.spring.boot.annotation.EventMapping;
import com.linecorp.bot.spring.boot.annotation.LineMessageHandler;

import com.linecorp.bot.model.Multicast;
import com.linecorp.bot.model.action.CameraAction;
import com.linecorp.bot.model.action.CameraRollAction;
import com.linecorp.bot.model.action.LocationAction;
import com.linecorp.bot.model.action.MessageAction;
import com.linecorp.bot.model.action.PostbackAction;
import com.linecorp.bot.model.action.URIAction;
import com.linecorp.bot.model.message.imagemap.ImagemapBaseSize;
import com.linecorp.bot.model.message.quickreply.QuickReply;
import com.linecorp.bot.model.message.quickreply.QuickReplyItem;
import com.linecorp.bot.model.message.template.ButtonsTemplate;
import com.linecorp.bot.model.message.template.CarouselColumn;
import com.linecorp.bot.model.message.template.CarouselTemplate;
import com.linecorp.bot.model.message.template.ConfirmTemplate;

public class QuickReplySetPolaMakan {

    public QuickReplySetPolaMakan() {

    }

    public TextMessage constructFrekuensiMakan() {
        List<QuickReplyItem> items = asList(
            QuickReplyItem.builder()
                          .action(new MessageAction("1", "1"))
                          .build(),
            QuickReplyItem.builder()
                          .action(new MessageAction("2", "2"))
                          .build(),
            QuickReplyItem.builder()
                          .action(new MessageAction("3", "3"))
                          .build(),
            QuickReplyItem.builder()
                          .action(new MessageAction("4", "4"))
                          .build(),
            QuickReplyItem.builder()
                          .action(new MessageAction("5", "5"))
                          .build()
        );

        TextMessage target = TextMessage
                .builder()
                .text("Frekuensi anda makan dalam sehari (perkiraan)")
                .quickReply(QuickReply.items(items))
                .build();

		return target;
    }

    public TextMessage constructDaftarMakanan() {
        List<QuickReplyItem> items = asList(
            QuickReplyItem.builder()
                          .action(new MessageAction("Nasi", "Nasi"))
                          .build(),
            QuickReplyItem.builder()
                          .action(new MessageAction("Mie", "Mie"))
                          .build(),
            QuickReplyItem.builder()
                          .action(new MessageAction("Kentang", "Kentang"))
                          .build(),
            QuickReplyItem.builder()
                          .action(new MessageAction("Daging", "Daging"))
                          .build(),
            QuickReplyItem.builder()
                          .action(new MessageAction("Ayam", "Ayam"))
                          .build(),
            QuickReplyItem.builder()
                          .action(new MessageAction("Ikan", "Ikan"))
                          .build(),
            QuickReplyItem.builder()
                          .action(new MessageAction("Kol", "Kol"))
                          .build(),
            QuickReplyItem.builder()
                          .action(new MessageAction("Timun", "Timun"))
                          .build(),
            QuickReplyItem.builder()
                          .action(new MessageAction("Selada", "Selada"))
                          .build(),
            QuickReplyItem.builder()
                          .action(new MessageAction("Selesai", "Selesai"))
                          .build()
        );

        TextMessage target = TextMessage
                .builder()
                .text("Pilih Menu Yang Biasa Anda Makan, Jika sudah pilih 'Selesai'")
                .quickReply(QuickReply.items(items))
                .build();

		return target;
    }
}
