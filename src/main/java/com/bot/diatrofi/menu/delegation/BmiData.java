package com.bot.diatrofi.menu.delegation;

import java.util.Map;
import java.util.HashMap;

public class BmiData {
    public Map<String,Float> weightMap;
    public Map<String,Float> heightMap;

    public BmiData(){
        this.weightMap = new HashMap<>();
        this.heightMap = new HashMap<>();
    }

}