package com.bot.diatrofi.menu.delegation;

public class DiatrofiHelpCommand extends DiatrofiDelegate {
    
    public DiatrofiHelpCommand() {
        super();
    }
    
    public DiatrofiHelpCommand(String[] command) {
        super(command);
        readMessage();
    }

    @Override
    public void readMessage() {
        if (command.length == 1) {
            reply = getGeneralText();
        } else if (command.length == 4) {
            if (commandStr.contains("set pola")) {
                reply = setPolaHelper();  
            } else {
                reply = getDefaultText();
            }
        } else if (command.length == 3) {
            if (commandStr.contains("set bmi")) {
                reply = getBmiText();
            } else if (commandStr.contains("lihat bmi")) {
                reply = getLookText();
            } else {
                reply = getDefaultText();
            }
        } else if (command.length == 2) {
            if (command[1].equals("health-o-meter")) {
                reply = getHealthText();
            } else {
                reply = getDefaultText();
            }
        } else {
            reply = getDefaultText();
        }
    }
    
    public String setPolaHelper() {
        switch(command[3]) {
        case "hidup" : return getPolaHidupText();
        case "makan" : return getPolaMakanText();
        case "olahraga" : return getPolaOlahragaText(); 
        case "tidur" : return getPolaTidurText();
        default : return getDefaultText();
        }
    }
    
//  --------------------------------------- Method Untuk Isi Teks Help Saat Dipanggil ---------------------------------------
    public String getGeneralText() {
        return "Diatrofi Bot adalah sebuah Line Chatbot yang memiliki misi membuat hidup orang menjadi semakin sehat.\n"
                + "Bot ini akan menerima input berupa pola makan, pola jam tidur dan jam olahraga User untuk kemudian"
                + "dianalisis dan memberikan feedback berupa level kesehatan pola hidup User.\n"
                + "Untuk cara penggunaan Bot lebih lanjut, silakan memasukkan perintah dibawah ini :\n \n"
                + "/help set pola hidup\n"
                + "/help set pola makan\n"
                + "/help set pola tidur\n"
                + "/help set pola olahraga\n"
                + "/help set bmi\n"
                + "/help lihat bmi\n"
                + "/help health-o-meter\n \n"
                + "Untuk menggunakan perintah, hilangkan kata 'help' (contoh : /health-o-meter)";
    }
    
    public String getPolaHidupText() {
        return "Perintah ini meminta input pola makan, pola jam tidur dan jam olahraga dari User untuk kemudian"
                + "dianalisis dan memberitahu User apakah pola hidupnya sudah sehat atau belum."
                + "Perintah ini HARUS dijalankan pertama kali (selain help) sebelum User bisa mengakses perintah"
                + "lainnya.";
    }
    
    public String getPolaMakanText() {
        return "User menginput makanan yang paling sering dikonsumsi setiap minggunya.";
    }
    
    public String getPolaOlahragaText() {
        return "User menginput seberapa lama ia berolahraga setiap harinya";
    }
    
    public String getPolaTidurText() {
        return "User menginput seberapa lama ia tidur setiap harinya.";
    }
    
    public String getBmiText() {
        return "User menginput tinggi dan berat badannya untuk dihitung besar BMI-nya.";
    }
    
    public String getHealthText() {
        return "User mendapatkan feedback dari pola hidupnya dalam bentuk gambar";
    }
    
    public String getLookText() {
        return "User mendapatkan feedback berupa besar BMInya dan apakah user tergolong obesitas, normal atau terlalu kurus";
    }
    
    public String getDefaultText() {
        return "Maaf, perintah tidak dimengerti..";
    }

    @Override
    public void setMessage(String message) {
        this.commandStr = message;
        this.command = message.split(" ");
        readMessage();
    }
    
    
}
