package com.bot.diatrofi.menu.delegation;
import java.lang.Math;

public class DelegateBMI extends DiatrofiDelegate {
    
    public float bmi_val = 0f;
    BmiData bmidata;

    public DelegateBMI() {
        super();
    }
    
    public DelegateBMI(String[] command, String userId, BmiData bmidata) {
        super(command, userId);
        this.bmidata = bmidata;
        readMessage();
    }

    @Override
    public void readMessage() {
        if (this.command[0].equals("/set") && this.command[1].equals("bmi")) {
            this.reply = "Masukan berat badan dalam kilogram, contoh\n\n/berat 90";
        }
        else if (this.command[0].equals("/lihat") && this.command[1].equals("bmi")) {
            String indicator = "";
            String link_result = String.format("https://diatrofi-db.herokuapp.com/bmi/get/id/%s", userId);
            String tmp_json = readAPI(link_result);
            System.out.println("This is the link!\n.\n.\n.\n" + link_result);
            System.out.println("This is the json!\n.\n.\n.\n" + tmp_json);
            String[] tmp_json_arr = tmp_json.split(",");
            int tmp_int = Integer.valueOf(tmp_json_arr[3].substring(6,8));
            if (tmp_int < 19) {
                indicator = " Kamu terlalu kurus :(";
            }
            else if (tmp_int >= 19 && tmp_int < 25) {
                indicator = " Kamu normal :)";
            }
            else if (tmp_int >= 25 && tmp_int < 30) {
                indicator = " Kamu overweight :(";
            }
            else if (tmp_int >= 30) {
                indicator = " Kamu obesitas :(\n" + " semangat untuk diet ya!";
            }
            else {
                indicator = " maaf kami gagal menemukan BMI anda";
            }
            this.reply = " Bmi kamu: " + tmp_json_arr[3].substring(6,8) + "\n\n" + indicator;
        }
        else if (this.command[0].equals("/berat")) {
            Float tmp_weight = new Float(command[1]+"f");
            this.bmidata.weightMap.put(userId, tmp_weight);

            this.reply = "Masukan tinggi badan dalam centimeter, contoh\n\n/tinggi 170";
        }
        else if (this.command[0].equals("/tinggi")) {
            Float tmp_height = new Float(command[1]+"f");
            this.bmidata.heightMap.put(userId, tmp_height);
            String indicator = "";
            
            Float real_weight = this.bmidata.weightMap.get(userId);
            Float real_height = this.bmidata.heightMap.get(userId);

            this.bmi_val = bmi_calculation(real_weight, real_height);
            if (this.bmi_val < 18.50f) {
                indicator = " Kamu terlalu kurus :(";
            }
            else if (this.bmi_val >= 18.50f && this.bmi_val <= 24.99f) {
                indicator = " Kamu normal :)";
            }
            else if (this.bmi_val >= 25.00f && this.bmi_val <= 29.99f) {
                indicator = " Kamu overweight :(";
            }
            else if (this.bmi_val >= 30.00f) {
                indicator = " Kamu obesitas :(\n" + " semangat untuk diet ya!";
            }
            else {
                indicator = " maaf kami gagal menemukan BMI anda";
            }

            int stored_bmi = Math.round(this.bmi_val);
            String bmi_string = String.valueOf(stored_bmi);

            this.reply = " nilai BMI kamu: " + bmi_string + "\n\n" + indicator;

            String result = String.format("https://diatrofi-db.herokuapp.com/bmi/post/id/%s/height/%d/weight/%d/bmi/%s",
            userId, Math.round(real_height), Math.round(real_weight), bmi_string);
            System.out.println(result);
            sendAPI(result);

        }
    }

    public float bmi_calculation(float weight, float height) {
        float result;
        float tmp_height;
        float tmp_weight;
        tmp_weight = weight;
        tmp_height = height/100;
        tmp_height = new Float(Math.pow(tmp_height, 2f));
        result = tmp_weight/tmp_height;
        return result;
    }

    @Override
    public void setMessage(String message) {
        this.commandStr = message;
        this.command = message.split(" ");
        readMessage();
    }
    
}


