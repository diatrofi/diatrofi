package com.bot.diatrofi.menu.delegation;

public class DelegateSetPolaMakan extends DiatrofiDelegate {

    public DelegateSetPolaMakan(String[] command, String userId) {
        super(command, userId);
        readMessage();
    }

    @Override
    public void readMessage() {
        if (command[1].equals("pola") && command[2].equals("makan")) {
            // perform get data from db bmi
            this.reply = "polamakan";
        }
        else if (command[0].equals("1") ||
                 command[0].equals("2") ||
                 command[0].equals("3") ||
                 command[0].equals("4") ||
                 command[0].equals("5")) {
            this.reply = command[0];
        }
        return;
    }

    @Override
    public void setMessage(String msg) {
        return;
    }
}
