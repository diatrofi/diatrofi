package com.bot.diatrofi.menu.delegation;

import com.linecorp.bot.model.action.MessageAction;
import com.linecorp.bot.model.message.TextMessage;
import com.linecorp.bot.model.message.quickreply.QuickReply;
import com.linecorp.bot.model.message.quickreply.QuickReplyItem;

import java.util.List;

import static java.util.Arrays.asList;

public class DelegatePolaHidup {

    public TextMessage SetPolaOlahraga() {
        List<QuickReplyItem> items1 =
                asList(QuickReplyItem.builder()
                                .action(new MessageAction("< 2 jam", "kurang dari 2 jam"))
                                .build(),
                        QuickReplyItem.builder()
                                .action(new MessageAction("2 - 4 jam", "2 hingga 4 jam"))
                                .build(),
                        QuickReplyItem.builder()
                                .action(new MessageAction("> 4 jam", "lebih dari 4 jam"))
                                .build()

                );

        TextMessage target = TextMessage
                .builder()
                .text("Berapa lama kamu berolahraga dalam seminggu ?")
                .quickReply(QuickReply.items(items1))
                .build();

        return target;
    }
    public TextMessage SetJenisOlahraga() {
        List<QuickReplyItem> items2 =
                asList(QuickReplyItem.builder()
                                .action(new MessageAction("Push Up", "Push Up"))
                                .build(),
                        QuickReplyItem.builder()
                                .action(new MessageAction("Jogging", "Jogging"))
                                .build(),
                        QuickReplyItem.builder()
                                .action(new MessageAction("Bersepeda", "Bersepeda"))
                                .build(),
                        QuickReplyItem.builder()
                                .action(new MessageAction("Kegiatan Olahraga", "Mengikuti Unit Kegiatan Olahraga seperti sepakbola, voli, basket, dll"))
                                .build(),
                        QuickReplyItem.builder()
                                .action(new MessageAction("Olahraga ringan", "Olahraga ringan"))
                                .build()

                );

        TextMessage target = TextMessage
                .builder()
                .text("Olahraga apa yang kamu lakukan ?  note : jika tidak terdapat di pilihan yang tersedia, silahkan pilih yang menyerupai ya")
                .quickReply(QuickReply.items(items2))
                .build();

        return target;
    }
    public String FinalAnswerOlahraga() {
        String text = "Baik, data diterima, jangan lupa berolahraga hari ini ya !";
        return text;
    }
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public TextMessage SetPolaTidur() {
        List<QuickReplyItem> items =
                asList(QuickReplyItem.builder()
                                .action(new MessageAction("< 4 jam", "kurang dari 4 jam"))
                                .build(),
                        QuickReplyItem.builder()
                                .action(new MessageAction("4 - 7 jam", "4 hingga 7 jam"))
                                .build(),
                        QuickReplyItem.builder()
                                .action(new MessageAction("> 7 jam", "lebih dari 7 jam"))
                                .build()
                );

        TextMessage target = TextMessage
                .builder()
                .text("Berapa lama kamu tidur dalam sehari ?")
                .quickReply(QuickReply.items(items))
                .build();

        return target;
    }
    public String PrintPolaTidurKurang4jam() {
        String text = "Baik, data diterima. \n" +
                "Saya sarankan kamu tidur yang cukup, mungkin bisa dimulai dari time management yang baik \n" +
                "Baca juga : \n" +
                "https://hellosehat.com/hidup-sehat/tips-sehat/9-cara-memperbaiki-pola-tidur-yang-berantakan";
        return text;
    }

    public String PrintPolaTidur4Hingga7jam() {
        String text = "Baik, data diterima. \n" +
                "Waktu tidur kamu sudah baik, pertahankan ya !";
        return text;
    }

    public String PrintPolaTidurLebih7jam() {
        String text = "Baik, data diterima. \n" +
                "Saya sarankan kamu untuk tidak tidur terlalu lama agar kesehatan kamu tetap terjaga. \n" +
                "Baca juga : \n" +
                "https://hellosehat.com/hidup-sehat/fakta-unik/6-efek-buruk-tidur-terlalu-lama/";
        return text;
    }
}
