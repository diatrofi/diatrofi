package com.bot.diatrofi.menu.delegation;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

    public abstract class DiatrofiDelegate {
    String[] command;
    String commandStr;
    String reply;
    String userId;
    
    public DiatrofiDelegate() {
        
    }
    
    public DiatrofiDelegate(String[] command) {
        this.command = command;
        this.commandStr = createString();
    }
    
    public DiatrofiDelegate(String[] command, String userId) {
        this.command = command;
        this.commandStr = createString();
        this.userId = userId;
    }
    
    abstract public void readMessage();

    public String readAPI (String link) {
        
        String output = "";
        
        try {
          URL url = new URL (link);
          HttpURLConnection conn = (HttpURLConnection) url.openConnection();
          conn.setRequestMethod("GET");
          conn.setRequestProperty("Accept", "Application/json");
          
          if (conn.getResponseCode() != 200) {
              throw new RuntimeException ("Failed : HTTP error code : " + conn.getResponseCode());
          }
          
          BufferedReader br = new BufferedReader (new InputStreamReader(conn.getInputStream()));
          output = br.readLine();
          
        } catch (MalformedURLException e) {
            output = "error URL";
        } catch (IOException e) {
            output = "error input";
        }
        return output;
    }
    
    /**
     * 
     * @param link
     * @return berhasil kalau URL benar, error kalau URL salah
     */
    public String sendAPI (String link) {
        String output = "";
        
        try {
            URL url = new URL (link);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            
            if (conn.getResponseCode() != 200) {
                throw new IOException ("Failed : HTTP error code : " + conn.getResponseCode());
            }
            
            output = "berhasil";
            
            conn.disconnect();
        } catch (MalformedURLException e) {
            output = "error URL";
        } catch (IOException e) {
            output = "error input";
        }
        return output;
    }
    
    public String replyMessage() {
        return reply;
    }
    
    abstract public void setMessage(String message);
    
    public String createString() {
        String res = "";
        for (int i = 0; i < command.length; i++) {
            if (i == (command.length-1)) {
                res += command[i];
            }
            res += command[i] + " ";
        }
        return res;
    }
}
