package com.bot.diatrofi.menu.delegation;

public class DelegateHealthMeter extends DiatrofiDelegate {
    int kalori, tidur, makan, olahraga;
    public DelegateHealthMeter() {
        super();
        this.kalori = 0;
        this.tidur = 0;
        this.makan = 0;
        this.olahraga = 0;
    }

    public DelegateHealthMeter(String[] command) {
        super(command);
        readMessage();
    }

    public DelegateHealthMeter(String[] command, String userId) {
        super(command, userId);
        readMessage();
    }

    /**
     * Setter data untuk milih gambar health meter
     */
    public void setHealth(int kalori, int tidur, int olahraga) {
        this.kalori = kalori;
        this.tidur = tidur;
        this.olahraga = olahraga;
   }

   public void setKalori(int kalori) {
       this.kalori = kalori;
   }

   public void setTidur(int tidur) {
       this.tidur = tidur;
   }

   public void setOlahraga(int olahraga) {
       this.olahraga = olahraga;
   }

   public void setMakan(int makan) {
       this.makan = makan;
   }

    @Override
    public void readMessage() {
        this.reply = tempFunction();
        replyMessage();
    }

    @Override
    public void setMessage(String msg) {
        return;
    }

    private String getImageFromDb(String id) {
        return "";
    }

    private String tempFunction() {
        return "https://i.paste.pics/61c732db168a0a6423098789a7161810.png";
    }

    private void getImage(String userId) {
        String link = 
            "https://diatrofi-db.herokuapp.com/pola-hidup/get/id/" + userId;
        String jsonData = readAPI(link);
        
        // splitting
        String[] stage1 = jsonData.split(",");
        String[] stage2 = stage1[4].split(":");
        String image = stage2[1];
        String image_final = image.substring(1, image.length());  // delete double quote
        String imageUrl = "https://i.paste.pics/" + image_final + ".png";
        this.reply = imageUrl;
    }

    public void setImage(String id, int tidur, int olahraga) {
        String http = "https://i.paste.pics/";
        String urls = "adb6976ec591cee8ad1bb01bd0290c9c;" +    // red
        "61c732db168a0a6423098789a7161810;" +      // yellow
        "d25a36be8df368751d3bf12ecc84bfcb";        // green
        String url[] = urls.split(";");

        // TODO pilih link berdasarkan stat dari db
        // TODO dapetin bmi pake userID
        int idx = chooseHealth();
        String chosenImage = url[idx];
        
        // Link yang mau disimpen
        String urlToSend = String.format(
            "https://diatrofi-db.herokuapp.com/pola-hidup/post/id/%s/kalori/%d/tidur/%d/olahraga/%d/health/%s",
             userId, this.kalori, this.tidur, this.olahraga, chosenImage);
        String send = sendAPI(urlToSend);
        this.reply = send;  // suksse atau ngga
    }

    public int chooseHealth() {
        // TODO olah data
        int score = 0;
        // OLAHRAGA //
        if (this.olahraga < 2)  score += 1;
        else if (this.olahraga > 2 && this.olahraga < 4) score += 2;
        else score += 3;

        // TIDUR //
        if (this.tidur < 4) score += 1;
        else if (this.tidur > 4 && this.tidur < 7) score += 2;
        else score += 3;


        if (score == 2 || score == 3 || score == 4) return 0;
        else if (score == 5 || score == 6 || score == 7) return 1;
        else return 2;
    }
}