package com.bot.diatrofi;

import com.bot.diatrofi.setpolamakan.Dish;
import com.bot.diatrofi.setpolamakan.Meal;

import com.bot.diatrofi.setpolamakan.dishdecorator.DishDecorator;
import com.bot.diatrofi.setpolamakan.dishdecorator.Beef;
import com.bot.diatrofi.setpolamakan.dishdecorator.Cabbage;
import com.bot.diatrofi.setpolamakan.dishdecorator.Chicken;
import com.bot.diatrofi.setpolamakan.dishdecorator.Cucumber;
import com.bot.diatrofi.setpolamakan.dishdecorator.Fish;
import com.bot.diatrofi.setpolamakan.dishdecorator.Lettuce;
import com.bot.diatrofi.setpolamakan.dishdecorator.Noodle;
import com.bot.diatrofi.setpolamakan.dishdecorator.Potato;
import com.bot.diatrofi.setpolamakan.dishdecorator.Rice;


public class PolaMakanController {

    private Meal dish;
    private int frekuensiMakan;

    public PolaMakanController() {
        this.dish = new Dish();
    }

    public void addMeal(String dishcondiment) {
        switch (dishcondiment) {
            case "Nasi":
                dish = new Rice(dish);
            case "Kentang":
                dish = new Potato(dish);
            case "Mie":
                dish = new Noodle(dish);
            case "Selada":
                dish = new Lettuce(dish);
            case "Ikan":
                dish = new Fish(dish);
            case "Timun":
                dish = new Cucumber(dish);
            case "Ayam":
                dish = new Chicken(dish);
            case "Kol":
                dish = new Cabbage(dish);
            case "Daging":
                dish = new Beef(dish);
        }
    }

    public void setFrekuensiMakan(int frekuensiMakan) {
    	this.frekuensiMakan = frekuensiMakan;
    }

    public int getFrekuensiMakan() {
    	return frekuensiMakan;
    }

    public double getCaloriesPerMeal() {
        return dish.calories();
    }
}
