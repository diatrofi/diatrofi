package com.bot.diatrofi.menu.delegation;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class DiatrofiHelpCommandTest {
    private DiatrofiDelegate helpCommand;           // for 1 use case
    private DiatrofiDelegate helpCommandCustom = new DiatrofiHelpCommand();     // for multiple usage
    
    @Before
    public void SetUp() {
        helpCommand = new DiatrofiHelpCommand("/help".split(" "));
    }
    
    @Test
    public void generalHelpTest() {
        assertEquals(helpCommand.replyMessage().contains("Diatrofi Bot") , true);
    }
    
    @Test
    public void setHelpHidupTest() {
        helpCommandCustom.setMessage("/help set pola hidup");
        assertEquals(helpCommandCustom.replyMessage().contains("input pola makan, pola jam tidur dan jam olahraga"),
                true);
    }
    
    @Test
    public void setHelpMakanTest() {
        helpCommandCustom.setMessage("/help set pola makan");
        assertEquals(helpCommandCustom.replyMessage(), "User menginput makanan yang paling sering dikonsumsi setiap minggunya.");
    }
    
    @Test
    public void setHelpOlahragaTest() {
        helpCommandCustom.setMessage("/help set pola olahraga");
        assertEquals(helpCommandCustom.replyMessage(), "User menginput seberapa lama ia berolahraga setiap harinya");
    }
    
    @Test
    public void setHelpTidurTest() {
        helpCommandCustom.setMessage("/help set pola tidur");
        assertEquals(helpCommandCustom.replyMessage(), "User menginput seberapa lama ia tidur setiap harinya.");
    }
    
    @Test
    public void setHelpBmiTest() {
        helpCommandCustom.setMessage("/help set bmi");
        assertEquals(helpCommandCustom.replyMessage(), "User menginput tinggi dan berat badannya untuk dihitung besar BMI-nya.");
    }
    
    @Test
    public void healthHelpTest() {
        helpCommandCustom.setMessage("/help health-o-meter");
        assertEquals(helpCommandCustom.replyMessage(), "User mendapatkan feedback dari pola hidupnya dalam bentuk gambar");
    }
    
    @Test
    public void lookHelpBmiTest() {
        helpCommandCustom.setMessage("/help lihat bmi");
        assertEquals(helpCommandCustom.replyMessage(), "User mendapatkan feedback berupa besar BMInya dan apakah user tergolong obesitas, normal atau terlalu kurus");
    }
    
// ------------------------------------------ START OF DEFAULT TEST CASE ------------------------------------------
    
    @Test
    public void helpMoreThan4WordTest() {
        helpCommandCustom.setMessage("/help ini lebih dari 4 kata");
        assertEquals(helpCommandCustom.replyMessage(), "Maaf, perintah tidak dimengerti..");
    }
    
    @Test 
    public void wrongHelp4WordTest() {
        helpCommandCustom.setMessage("/help set pola salah");
        assertEquals(helpCommandCustom.replyMessage(), "Maaf, perintah tidak dimengerti..");
        
        helpCommandCustom.setMessage("/help set perintah salah");
        assertEquals(helpCommandCustom.replyMessage(), "Maaf, perintah tidak dimengerti..");
        
        helpCommandCustom.setMessage("/help ini perintah salah");
        assertEquals(helpCommandCustom.replyMessage(), "Maaf, perintah tidak dimengerti..");
    }
    
    @Test
    public void wrongHelp3WordTest() {
        helpCommandCustom.setMessage("/help set salah");
        assertEquals(helpCommandCustom.replyMessage(), "Maaf, perintah tidak dimengerti..");
        
        helpCommandCustom.setMessage("/help perintah salah");
        assertEquals(helpCommandCustom.replyMessage(), "Maaf, perintah tidak dimengerti..");
        
    }
    
    @Test
    public void wrongHelp2WordTest() {
        helpCommandCustom.setMessage("/help salah");
        assertEquals(helpCommandCustom.replyMessage(), "Maaf, perintah tidak dimengerti..");
    }
}
